use anyhow::{anyhow, Context as _, Result};
use apply::Apply as _;
use camino::{Utf8Path, Utf8PathBuf};
use log::info;
use path_ext::Utf8PathExt as _;
use pgp::crypto::sym::SymmetricKeyAlgorithm;
use pgp::ser::Serialize as _;
use pgp::types::StringToKey;
use rand::rngs::OsRng;
use std::io::Write as _;

pub async fn create<T, I, J>(
    archive_name: &str,
    files: I,
    exclude: J,
    password: &Option<String>,
) -> Result<Utf8PathBuf>
where
    T: AsRef<str>,
    I: IntoIterator<Item = T>,
    J: IntoIterator<Item = T>,
{
    // create output folder in case it doesn't exist
    archive_name
        .apply(Utf8PathBuf::from)
        .parent()
        .map(Utf8PathBuf::from)
        .unwrap_or_default()
        .apply(std::fs::create_dir_all)
        .with_context(|| format!("could not create output folder for {:?}", archive_name))?;

    let filename = format!(
        "{}--{}.tar.xz",
        archive_name,
        chrono::Utc::now().format("%Y-%m-%d--%H-%M-%S").to_string()
    );

    let archive_file = if let Some(passphrase) = password {
        encrypted_archive(&filename, files, exclude, passphrase).await
    } else {
        archive(&filename, files, exclude).await
    }?;

    archive_file
        .set_permissions(0o666)
        .await
        .with_context(|| format!("could not set permissions for file: {:?}", archive_file))?;

    Ok(archive_file)
}

async fn encrypted_archive<I, J, T>(
    filename: &str,
    files: I,
    exclude: J,
    passphrase: &str,
) -> Result<Utf8PathBuf>
where
    T: AsRef<str>,
    I: IntoIterator<Item = T>,
    J: IntoIterator<Item = T>,
{
    let encrypted_file = format!("{}.pgp", filename);

    info!("creating encrypted archive: {:?}", &encrypted_file);

    let mut tar_builder = Vec::new()
        .apply(std::io::Cursor::new)
        .apply(|obj| xz2::write::XzEncoder::new(obj, 9))
        .apply(tar::Builder::new);

    let files = walk_dirs(files, exclude).await?;
    for (path, name) in files {
        tar_builder
            .append_path_with_name(&path, &name)
            .with_context(|| format!("could not add {:?} to {:?}", &path, &encrypted_file))?;
    }

    let data = tar_builder
        .into_inner()
        .context("could not write tar data")?
        .finish()
        .context("could not write xz data")?
        .into_inner();

    let mut file = (&encrypted_file)
        .apply(std::fs::File::create)
        .with_context(|| format!("could not create file: {:?}", &encrypted_file))?
        .apply(std::io::BufWriter::new);

    pgp::message::Message::new_literal_bytes(filename, data.as_ref())
        .encrypt_with_password(
            &mut OsRng,
            StringToKey::new_default(&mut OsRng),
            SymmetricKeyAlgorithm::AES256,
            || passphrase.to_string(),
        )
        .with_context(|| format!("could not encrypt file: {:?}", &encrypted_file))?
        .to_writer(&mut file)
        .context("could not write pgp data")
        .map(|_| Utf8PathBuf::from(encrypted_file))
}

async fn archive<I, J, T>(filename: &str, files: I, exclude: J) -> Result<Utf8PathBuf>
where
    T: AsRef<str>,
    I: IntoIterator<Item = T>,
    J: IntoIterator<Item = T>,
{
    info!("creating archive: {:?}", filename);

    let mut tar_builder = filename
        .apply(std::fs::File::create)
        .with_context(|| format!("could not create file: {:?}", &filename))?
        .apply(std::io::BufWriter::new)
        .apply(|obj| xz2::write::XzEncoder::new(obj, 9))
        .apply(tar::Builder::new);

    for (path, name) in walk_dirs(files, exclude).await? {
        tar_builder
            .append_path_with_name(&path, &name)
            .with_context(|| format!("could not add {:?} to {:?}", &path, &filename))?;
    }

    // this is a bit much and probably stupid...
    tar_builder
        .into_inner()
        .context("could not write xz data")?
        .finish()
        .context("could not write to buffer")?
        .flush()
        .context("could not write to file")
        .map(|_| Utf8PathBuf::from(filename))
}

pub async fn walk_dirs<P, E, T>(paths: P, exclude: E) -> Result<Vec<(Utf8PathBuf, Utf8PathBuf)>>
where
    P: IntoIterator<Item = T>,
    E: IntoIterator<Item = T>,
    T: AsRef<str>,
{
    let exclude = exclude
        .into_iter()
        .map(|x| x.as_ref().to_string())
        .collect::<Vec<String>>();

    let mut queue: Vec<Utf8PathBuf> = Vec::new();

    let mut res = Vec::new();
    for path in paths.into_iter().map(|p| Utf8PathBuf::from(p.as_ref())) {
        queue.push(path.clone());

        while let Some(p) = queue.pop() {
            if p.is_file() {
                let name = path_and_name(&path, &p)?;
                res.push((p, name));
                continue;
            }

            p.directory_contents()
                .await
                .with_context(|| format!("could not list dir entries for {:?}", &p))?
                .into_iter()
                .filter(|f| exclude.iter().all(|ex| !f.ends_with(ex)))
                .for_each(|p| queue.push(p));
        }
    }
    Ok(res)
}

fn path_and_name<T: AsRef<Utf8Path>>(base_path: T, file: T) -> Result<Utf8PathBuf> {
    let b = base_path
        .as_ref()
        .to_path_buf()
        .parent()
        .map(Utf8PathBuf::from)
        .unwrap_or_else(Utf8PathBuf::new);
    let mut bi = b.components();

    let n = file
        .as_ref()
        .components()
        .skip_while(|c| Some(*c) == bi.next())
        .collect::<Utf8PathBuf>();

    Ok(if n.as_os_str().is_empty() {
        file.as_ref()
            .file_name()
            .ok_or_else(|| {
                anyhow!(
                    "could not get relative path from {:?} and {:?}",
                    base_path.as_ref(),
                    file.as_ref()
                )
            })?
            .apply(Utf8PathBuf::from)
    } else {
        n
    })
}
