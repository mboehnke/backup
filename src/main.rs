mod archive;
mod config;

use anyhow::Context;
use log::{error, info};

macro_rules! error_chain {
    ($error:expr) => {
        error!(
            "{}",
            $error
                .chain()
                .map(|x| x.to_string())
                .collect::<Vec<_>>()
                .join("\n")
        )
    };
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // load configuration
    let settings = config::get()?;

    // initialize logger
    env_logger::Builder::new()
        .parse_filters(&settings.loglevel)
        .init();

    loop {
        // (re)load configuration
        info!("loading configuration");
        let settings = config::get()?;

        for backup in settings.backups {
            let file = match archive::create(
                &backup.archive,
                &backup.files,
                &backup.exclude,
                &backup.password,
            )
            .await
            .with_context(|| format!("could not create archive {:?}", &backup.archive))
            {
                Ok(f) => f,
                Err(e) => {
                    error_chain!(e);
                    continue;
                }
            };

            if backup.upload {
                info!("uploading file to google drive: {:?}", &file);

                if let Err(e) = network::wait_for_internet_connection().await {
                    error!("{}", e)
                } else {
                    let client =
                        match google_api::DriveApi::new(&settings.credentials, &settings.token)
                            .await
                            .context("could not create google drive client")
                        {
                            Ok(c) => c,
                            Err(e) => {
                                error_chain!(e);
                                continue;
                            }
                        };
                    if let Err(e) = client.upload(&file).await.with_context(|| {
                        format!(
                            "could not upload file to google drive: {:?}",
                            &backup.archive
                        )
                    }) {
                        error_chain!(e);
                    }
                }
            }
        }

        // sleep between backups
        info!("sleeping for {}h", settings.hours_between_backups);
        std::thread::sleep(std::time::Duration::from_secs(
            60 * 60 * settings.hours_between_backups,
        ));
    }
}
