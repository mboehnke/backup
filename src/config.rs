use serde::Deserialize;
use std::error::Error;

// const SETTINGS_DIR: &str = "settings";
const SETTINGS_DIR: &str = "/var/lib/backup";
const SETTINGS_FILE: &str = "settings.toml";
const CREDENTIALS_FILE: &str = "client_secret.json";
const TOKEN_FILE: &str = "drive_token.json";

#[derive(Deserialize, Debug, Clone)]
pub struct Backup {
    pub archive: String,
    pub files: Vec<String>,
    pub exclude: Vec<String>,
    pub password: Option<String>,
    pub upload: bool,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Settings {
    pub credentials: String,
    pub token: String,
    pub loglevel: String,
    pub hours_between_backups: u64,
    pub backups: Vec<Backup>,
}

pub fn get() -> Result<Settings, Box<dyn Error>> {
    let s_file = [SETTINGS_DIR, SETTINGS_FILE].join("/");
    let c_file = [SETTINGS_DIR, CREDENTIALS_FILE].join("/");
    let t_file = [SETTINGS_DIR, TOKEN_FILE].join("/");

    let mut cfg = config::Config::new();

    cfg.merge(config::File::with_name(&s_file))?;
    cfg.set("credentials", c_file)?;
    cfg.set("token", t_file)?;

    cfg.try_into().map_err(Box::from)
}
