FROM rust as builder
COPY ./ ./
RUN cargo build --release

FROM ubuntu
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \ 
    apt-get install -y openssl ca-certificates tzdata liblzma5 && \
    ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata
COPY --from=builder ./target/release/backup /usr/bin/
CMD ["/usr/bin/backup"]
